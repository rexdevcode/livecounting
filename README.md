# Angular 2 RexDev starter seed

This repository has the base configuration for starting a project width
Angular 2 Rc.3 and foundation framework's

Author Cristian Ramirez

## Clone the repository

Git clone the project

<code>$ git clone </code>

## Install the Node and Bower dependencies

<code>$ npm i </code>

## Run the project

```
$ gulp
```

## Git branches

=> Master has the basic Foundation template starter

```
$ git checkout ZoomOut
```

=> ZoomOut has the ZoomOut template
