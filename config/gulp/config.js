'use strict';
const envConfig = require('./env');

module.exports = () => {
  // Rutas del proyecto
    const root       = '';
    const src        = root + 'src/';
    const app        = src + 'app/';
    const test       = src + 'test/';
    const tmp        = src + 'tmp/';
    const tmpApp     = tmp + 'app/';
    const tmpTest    = tmp + 'test/';
    const testHelper = test + 'test-helpers/';
    const e2e        = test + 'e2e/';
    const tmpE2E     = tmpTest + 'e2e/';
    const phpFiles   = src + 'php/';
    const assets     = src + 'assets/';
    const assetsPath = {
      styles: assets + 'styles/',
      images: assets + 'images/',
      fonts: assets + 'fonts/'
    };

    // Rutas de archivos principales
    const index   = src + 'index.html';
    const tsFiles = [
      app + '**/!(*.spec)+(.ts)'
    ];
    const tsTestFiles = {
        unit: [app + '**/*.spec.ts'],
         e2e: [e2e + '**/*.ts'],
      helper: [testHelper + '**/*.ts']
    };
    const typingFiles = [
        'typings/index.d.ts',
         src + 'manual_typings/**/*.d.ts'
    ];
    const sassFiles = {
         app: `${app}**/*.scss`,
      static: `${assetsPath.styles}**/*.scss`
    };
    const cssStyles = [
      app + '**/*.css',
      'bower_components/components-font-awesome/css/font-awesome.min.css',
      'bower_components/mdi/css/materialdesignicons.min.css',
      'bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
      'bower_components/owl.carousel/dist/assets/owl.theme.default.min.css'
    ];

    // Rutas de production
    const build = {
      path: 'build/',
      app: 'build/app/',
      fonts: 'build/fonts',
      assetPath: 'build/assets/',
      assets: {
        lib: {
            js: 'lib.js',
            css: 'lib.css'
        }
      }
    };
    const report = {
      path: 'report/'
    };

    const e2eConfig = {
      seleniumTarget: 'http://127.0.0.1:3000'
    };

    const systemJs = {
      builder: {
          normalize: true,
          minify: true,
          mangle: true,
          runtime: false,
          globalDefs: { DEBUG: false, ENV: 'production' }
      }
    };

    const config = {
            root: root,
             src: src,
             app: app,
            test: test,
             tmp: tmp,
          tmpApp: tmpApp,
         tmpTest: tmpTest,
          tmpE2E: tmpE2E,
      testHelper: testHelper,
             e2e: e2e,
       e2eConfig: e2eConfig,
        phpFiles: phpFiles,
          assets: assets,
           index: index,
           build: build,
          report: report,
      assetsPath: assetsPath,
         tsFiles: tsFiles,
      tsTestFiles: tsTestFiles,
      typingFiles: typingFiles,
       sassFiles: sassFiles,
       cssStyles: cssStyles,
        systemJs: systemJs
    };

    if (envConfig.ENV === envConfig.ENVS.DEV)
    {
        const historyApiFallback = require('connect-history-api-fallback');
        const browserSync = {
            dev: {
                port: 3000,
                server: {
                    baseDir: './src/',
                    middleware: [historyApiFallback()],
                    routes: {
                        '/node_modules': 'node_modules',
                        '/bower_components': 'bower_components',
                        '/src': 'src'
                    }
                },
                files: [
                    src + 'index.html',
                    src + 'systemjs.conf.js',
                    assetsPath.styles + 'main.css',
                    tmpApp + '**/*.js',
                    app + '**/*.css',
                    app + '**/*.html'
                ]
            },
            prod: {
                port: 3001,
                server: {
                    baseDir: './' + build.path,
                    middleware: [historyApiFallback()]
                }
            }
        };

        config.browserSync = browserSync;
    }

    return config;
};
