'use strict';
const gulp       = require('gulp');
const config     = require('../config')();
const ts         = require('gulp-typescript');
const tslint     = require('gulp-tslint');
const sourcemaps = require('gulp-sourcemaps');
const argv       = require('yargs').argv;

/* Initialize TS Project */
const tsUnitFiles = [].concat(config.tsTestFiles.unit, config.tsTestFiles.helper);
const tsE2EFiles = [].concat(config.tsTestFiles.e2e, config.tsTestFiles.helper);
const tsFiles = [].concat(config.tsFiles, tsUnitFiles, tsE2EFiles);

/* Watch changed typescripts file and compile it */
gulp.task('watch-ts', () => {
    return gulp.watch(tsFiles, (file) => {
        console.log('Compiling ' + file.path + '...');
        return compileTs(file.path, true);
    });
});

/* Compile typescripts */
gulp.task('tsc', ['clean-ts'], () => {
    return compileTs(tsFiles);
});

gulp.task('tsc-app', ['clean-ts-app'], () => {
    return compileTs(config.tsFiles);
});

gulp.task('tsc-unit', ['clean-ts-test'], () => {
    return compileTs(tsUnitFiles);
});

gulp.task('tsc-e2e', ['clean-ts-test'], () => {
    return compileTs(tsE2EFiles);
});

/* Lint typescripts */
gulp.task('tslint', () => {
    return lintTs(tsFiles);
});

gulp.task('tslint-app', () => {
    return lintTs(config.tsFiles);
});

gulp.task('tslint-unit', () => {
    return lintTs(tsUnitFiles);
});

gulp.task('tslint-e2e', () => {
    return lintTs(tsE2EFiles);
});

function lintTs(files) {
    return gulp.src(files)
        .pipe(tslint())
        .pipe(tslint.report('prose', {
          summarizeFailureOutput: true
        }));
}

function compileTs(files, watchMode) {
    const inline = !argv.excludeSource;
    watchMode = watchMode || false;

    const tsProject = ts.createProject('tsconfig.json');
    const allFiles = [].concat(files, config.typingFiles);
    const res = gulp.src(allFiles, {
            base: config.src,
            outDir: config.tmp
        })
        // .pipe(tslint())
        // .pipe(tslint.report('prose', {
        //     summarizeFailureOutput: true,
        //     emitError: !watchMode
        // }))
        .pipe(sourcemaps.init())
        .pipe(ts(tsProject))
        .on('error', () => {
            if (watchMode) {
                return;
            }
            process.exit(1);
        });
    return res.js
        .pipe(sourcemaps.write('.', {
            includeContent: inline
        }))
        .pipe(gulp.dest(config.tmp));
}
