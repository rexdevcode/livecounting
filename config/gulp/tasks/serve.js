'use strict';
const envConfig = require('../env');

if (envConfig.ENV === envConfig.ENVS.DEV)
{
    const gulp = require('gulp');
    const config = require('../config')();
    const bs = require("browser-sync").create();
    const startBrowsersync = (config) => {
        bs.init(config);
        bs.reload();
    };

    /* Start live server dev mode */
    gulp.task('serve-dev', ['sass', 'tsc-app', 'watch-ts', 'watch-sass'], () => {
        startBrowsersync(config.browserSync.dev);
    });

    /* Start live server production mode */
    gulp.task('serve-build', ['build'], () => {
        startBrowsersync(config.browserSync.prod);
    });
}
