import { provideRouter, RouterConfig } from '@angular/router';

import {HomeComponent} from './components/home/home.component';
import {ContenidoComponent} from './components/contenido/contenido.component';

const routes: RouterConfig = [
  { path: '' ,         component: HomeComponent },
  { path: 'contenido', component: ContenidoComponent }
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
