import {Component, OnInit, AfterViewInit} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {MenuComponent} from './components/menu2/menu.component';
import {EmailService} from './components/services/email.service';

declare var $: any;
declare var foundation: any;

@Component({
    selector: 'rexdev-main-app',
    templateUrl: 'app/app.html',
    directives: [MenuComponent, ROUTER_DIRECTIVES],
    providers: [EmailService]
})
export class AppComponent implements OnInit{

  ngOnInit(){
    $(document).foundation();
  }


}
