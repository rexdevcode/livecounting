import {Component} from '@angular/core';
import {ContenidoComponent} from '../contenido/contenido.component';

@Component({
    selector: 'rexdev-home',
    templateUrl: 'app/components/home/home.html',
    directives: [ContenidoComponent]
})
export class HomeComponent {}
