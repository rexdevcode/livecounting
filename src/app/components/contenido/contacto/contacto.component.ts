import {Component, Input, AfterViewInit} from '@angular/core';
import {FormBuilder,FORM_DIRECTIVES,
        ControlGroup,
        Validators}           from '@angular/common';
import {EmailService}         from '../../services/email.service';
import {CustomValidators}     from '../../validators/custom-validators';

@Component({
    selector: 'rexdev-contacto',
    templateUrl: 'app/components/contenido/contacto/contacto.html',
    directives: [FORM_DIRECTIVES]
})

export class ContactoComponent implements AfterViewInit {

  @Input() fondo: string;
  public group: ControlGroup;
  public formulario:any = {};
  public mensaje: string;

  constructor(private _builder: FormBuilder, private _emailService: EmailService) {

    this.formulario = {
      nombre: '',
      email: '',
      asunto: '',
      telefono: '',
      mensaje: ''
    };

    this.group = this._builder.group({
      nombre: ['',
        Validators.compose([Validators.required, CustomValidators.textFormat])
      ],
      email: ['',
        Validators.compose([Validators.required, CustomValidators.emailFormat])
      ],
      asunto: ['',
        Validators.compose([Validators.required, CustomValidators.textFormat])
      ],
      telefono: ['',
        Validators.compose([Validators.required, CustomValidators.telefonoFormat])
      ],
      mensaje: ['',
        Validators.compose([Validators.required, CustomValidators.textAreaFormat])
      ]
    });

  }

  ngAfterViewInit() {
    // document.getElementById('contacto-background').style.backgroundImage = `url(${'../../../assets/images/cover/home2.jpg'})`;

  }

  submitForm() {
    this._emailService.send(this.formulario).subscribe(
      (data) => this.mensaje = data.mensaje,
      (err)  => console.log(err),
      ()     => {
        this.formulario = {
          nombre: '',
          email: '',
          asunto: '',
          telefono: '',
          mensaje: ''
        };
      }
    );
  }


}
