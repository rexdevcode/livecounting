import {Component} from '@angular/core';
import {NosotrosComponent} from './nosotros/nosotros.component';
import {ServiciosComponent} from './servicios/servicios.component';
import {PrecioComponent} from './precio/precio.component';
import {ContactoComponent} from './contacto/contacto.component';

@Component({
    selector: 'rexdev-contenido',
    templateUrl: 'app/components/contenido/contenido.html',
    directives: [NosotrosComponent,ServiciosComponent,ContactoComponent,PrecioComponent]
})
export class ContenidoComponent {}
