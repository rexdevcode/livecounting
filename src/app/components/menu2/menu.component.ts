import {Component, Input, ChangeDetectionStrategy, AfterViewInit} from '@angular/core';
import {CORE_DIRECTIVES} from '@angular/common';
import {ROUTER_DIRECTIVES} from '@angular/router';

declare var $: any;

@Component({
    selector: 'rexdev-menu',
    templateUrl: 'app/components/menu2/menu.html',
    directives: [ROUTER_DIRECTIVES, CORE_DIRECTIVES]
})
export class MenuComponent implements AfterViewInit{

  ngAfterViewInit(){
    $('.cd-main-nav').on('click', function(event){
    	if($(event.target).is('.cd-main-nav')) $(this).children('ul').toggleClass('is-visible');
    });
  }
}
