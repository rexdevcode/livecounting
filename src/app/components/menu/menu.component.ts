import {Component, Input, ChangeDetectionStrategy, AfterViewInit} from '@angular/core';
import {CORE_DIRECTIVES} from '@angular/common';
import {ROUTER_DIRECTIVES} from '@angular/router';

declare var $: any;

@Component({
    selector: 'rexdev-menu',
    templateUrl: 'app/components/menu/menu.html',
    directives: [ROUTER_DIRECTIVES, CORE_DIRECTIVES]
})
export class MenuComponent implements AfterViewInit{

  ngAfterViewInit(){
    var projectsContainer = $('.cd-projects-container'),
  		navigation = $('.cd-primary-nav'),
  		triggerNav = $('.cd-nav-trigger'),
  		logo = $('.cd-logo');

  	triggerNav.on('click', function(){
      $('.cd-bg-1').toggleClass('disabled');
  		if( triggerNav.hasClass('project-open') ) {
  			//close project
  			projectsContainer.removeClass('project-open').find('.selected').removeClass('selected').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
  				$(this).children('.cd-project-info').scrollTop(0).removeClass('has-boxshadow');

  			});
  			triggerNav.add(logo).removeClass('project-open');
  		} else {
  			//trigger navigation visibility
  			triggerNav.add(projectsContainer).add(navigation).toggleClass('nav-open');
  		}
  	});


  }

}
