'use strict';
(function(global) {
    // ENV
    global.ENV = global.ENV || 'development';

    // map tells the System loader where to look for things
    var map = {
        'app': 'src/tmp/app',
        'test': 'src/tmp/test'
    };

    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        'app': {
            defaultExtension: 'js'
        },
        'test': {
            defaultExtension: 'js'
        },
        'rxjs': {
            defaultExtension: 'js'
        },
        'angular2-google-maps/core': {
          defaultExtension: 'js'
        }
    };

    // List bower packages here
    var bowerPackages = [
        'foundation-sites',
        'motion-ui',
        'jquery',
        'what-input'
    ];

    // List npm packages here
    var npmPackages = [
        '@angular',
        'rxjs',
        'angular2-google-maps',
        'lodash'
    ];

    // Add package entries for packages that expose barrels using index.js
    var packageNames = [
        // 3rd party barrels
        '@angular/router',
        'angular2-google-maps',
        'lodash'
    ];

    // Add package entries for angular packages
    var ngPackageNames = [
        'common',
        'compiler',
        'core',
        'forms',
        'http',
        'platform-browser',
        'platform-browser-dynamic'
    ];

    bowerPackages.forEach(function(pkgName) {
      map[pkgName] = `bower_modules/${pkgName}`;
    });

    bowerPackages.forEach(function(pkgName) {
      packages[pkgName] = { main: `/dist/${pkgName}.min.js`, defaultExtension: 'js' };
    });

    npmPackages.forEach(function (pkgName) {
        map[pkgName] = 'node_modules/' + pkgName;
    });

    packageNames.forEach(function(pkgName) {
        packages[pkgName] = { main: 'index.js', defaultExtension: 'js' };
    });

    ngPackageNames.forEach(function(pkgName) {
        var main = global.ENV === 'testing' ? 'index.js' :
            'bundles/' + pkgName + '.umd.js';

        packages['@angular/'+pkgName] = { main: main, defaultExtension: 'js' };
    });

    var config = {
        map: map,
        packages: packages
    };

    // filterSystemConfig - index.html's chance to modify config before we register it.
    if (global.filterSystemConfig) { global.filterSystemConfig(config); }

    System.config(config);

})(this);
