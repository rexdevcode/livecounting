'use strict';
const gulp       = require('gulp');
const requireDir = require('require-dir');
const tasks      = requireDir('./config/gulp/tasks');
const envConfig  = require('./config/gulp/env');

console.log('============ Angular 2 Starter ============');
console.log('Current environment: ' + envConfig.ENV);
console.log('- Change environment via --env or NODE_ENV');
console.log('===========================================');

/* Default task */
gulp.task('default', ['serve-dev']);
